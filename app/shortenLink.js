const express = require('express');
const path = require('path');
const nanoid = require('nanoid');
const ShortenLink = require('../models/ShortenLink');

const router = express.Router();

const createRouter = () => {
  router.get('/:shortenLink', (req, res) => {
    ShortenLink.find({shortenLink: req.params.shortenLink})
      .then(result => res.status(301).redirect(result[0].link))
      .catch(error => res.status(400).send(error))
  });

  router.post('/links', (req, res) => {
    const user_data = {
      link: req.body.url,
      shortenLink: nanoid(10)
    }

    const shortenLink = new ShortenLink(user_data);

    console.log(user_data);
    shortenLink.save()
      .then(result => res.send({...result, shortenLink: `http://localhost:8000/${result.shortenLink}`}))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;
