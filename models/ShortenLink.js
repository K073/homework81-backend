const mongoose = require('mongoose');

const shortenLinkSchema = new mongoose.Schema({
  link: {
    type: String, required: true
  },
  shortenLink: {
    type: String, required: true
  }
});

const ShortenLink = mongoose.model('ShortenLink', shortenLinkSchema);

module.exports = ShortenLink;
